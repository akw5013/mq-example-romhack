from character_map import *
import utils

class character_data:
    name_bytes = 8
    def __init__(self, nameOffset):
        self.nameOffset = nameOffset
    
    def set_name(self, writer, name):
        padded_name = name.ljust(self.name_bytes, ' ')
        writer.write_to(utils.get_byte_offset(writer.hasHeader, self.nameOffset), utils.bytes_as_base64(text_to_bytes(padded_name)))

Benjamin = character_data(0x0650B0)
Kaeli1   = character_data(0x065100)
Kaeli2   = character_data(0x065240)
Tristam1 = character_data(0x065150)
Tristam2 = character_data(0x065290)
Phoebe1  = character_data(0x0651A0)
Phoebe2  = character_data(0x0652E0)
Reuben1  = character_data(0x0651F0)
Reuben2  = character_data(0x065230)