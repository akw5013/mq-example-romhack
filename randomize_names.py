from character_data import *

def randomize_names(writer, names):
    for name,char in [['Kaeli', Kaeli1], ['Kaeli', Kaeli2], [ 'Tristam', Tristam1 ], [ 'Tristam', Tristam2 ], [ 'Reuben', Reuben1 ], [ 'Reuben', Reuben2 ], [ 'Phoebe', Phoebe1 ], [ 'Phoebe', Phoebe2]]:
        char.set_name(writer, names[name])
    return