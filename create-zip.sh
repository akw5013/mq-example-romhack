#!/bin/bash
rm -rf dist
rm output.zip
pip install --target ./dist -r requirements.txt
cp *.py *.txt *.tbl ./dist
# gonna be different if you're on linux - use zip instead
cd dist
7z.exe a ../output.zip .
