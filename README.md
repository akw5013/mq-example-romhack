To install dependencies
```
pip install --target ./package jsonpickle
```

To run
```
python lambda_function.py
```

To deploy: Zip contents of code + package directory and upload to AWS
