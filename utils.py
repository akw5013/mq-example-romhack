import base64

HEADER_LENGTH = 0x200

def int16_as_bytes(value):
    value = value & 0xFFFF
    return [value & 0xFF, (value >> 8) & 0xFF]

def int32_as_bytes(value):
    value = value & 0xFFFFFFFF
    return [value & 0xFF, (value >> 8) & 0xFF, (value >> 16) & 0xFF, (value >> 24) & 0xFF]

def bytes_as_base64(b):
    base64_bytes = base64.b64encode(b)    
    return base64_bytes.decode('utf8')

def get_byte_offset(hasHeader, offset):
    if(hasHeader):
        return offset + HEADER_LENGTH
    else:
        return offset