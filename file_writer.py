import json

class Writer:
    def __init__(self, hasHeader):
        self.instructions = []
        self.hasHeader = hasHeader
    def write_to(self, location, base64):
        self.instructions.append(Instruction(location, base64))

    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)

class Instruction:
    def __init__(self, location, base64):
        self.location = location
        self.base64 = base64
    
    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    def toString(self):
        return f'{self.location}:{self.base64}'

