mq_character_encoding = { }
for line in open('mq.tbl'):
    a, b = line.strip("\n").split("=")
    a = int(a, 0x10)
    mq_character_encoding[a] = b
    mq_character_encoding[b] = a

def text_to_bytes(chars):
    array = []
    for char in chars:
        array.append(mq_character_encoding[char] if char in mq_character_encoding else 3)
    return bytes(array)

def bytes_to_text(data):
    return "".join([mq_character_encoding[d] if d in mq_character_encoding else "~" for d in data])
