import logging
import jsonpickle
from file_writer import *
from randomize_names import *

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def buildBaseResponse():
    return {
            'statusCode': 200,
            'headers': {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST,OPTIONS,*'
            },
    }
def first(iterable, default=None):
    for item in iterable:
        return item
    return default

def lambda_handler(event, context):
    response = buildBaseResponse()
    
    logger.info('## EVENT\r' + jsonpickle.encode(event))
    logger.info('## CONTEXT\r' + jsonpickle.encode(context))

    if (event['httpMethod'] == 'OPTIONS'):
        # Respond to CORS pre-flight request
        return response

    if (event['httpMethod'] == 'GET'):
        response['statusCode'] = 404
        return response

    writer = Writer(False)
    
    reqBody = jsonpickle.decode(event['body'])
    logger.info('## Event Body\r' + jsonpickle.encode(reqBody))
    reqBody = jsonpickle.decode(reqBody['body'])
    logger.info('## Event Body\r' + jsonpickle.encode(reqBody))

    names = { }
    KaeliFlag = first(x['output'] for x in reqBody['selectedFlags'] if x['name'] == 'Kaeli')
    TristamFlag = first(x['output'] for x in reqBody['selectedFlags'] if x['name'] == 'Tristam')
    ReubenFlag = first(x['output'] for x in reqBody['selectedFlags'] if x['name'] == 'Reuben')
    PhoebeFlag = first(x['output'] for x in reqBody['selectedFlags'] if x['name'] == 'Phoebe')
    names['Kaeli'] = 'Kaeli' if KaeliFlag is None else KaeliFlag
    names['Tristam'] = 'Tristam' if TristamFlag is None else TristamFlag
    names['Reuben'] = 'Reuben' if ReubenFlag is None else ReubenFlag
    names['Phoebe'] = 'Phoebe' if PhoebeFlag is None else PhoebeFlag

    randomize_names(writer, names)

    response['body'] = writer.toJson()
    return response

if __name__ == "__main__":
    testEvent = {
        "body": "{\"headers\":{\"Accept\":\"application/json\",\"Content-Type\":\"application/json\"},\"body\":\"{\\\"version\\\":\\\"1\\\",\\\"selectedFlags\\\":[{\\\"name\\\":\\\"Kaeli\\\",\\\"description\\\":\\\"Fill in Kaeli's Name\\\",\\\"mode\\\":\\\"text\\\",\\\"output\\\":\\\"Baelia\\\",\\\"validationRegex\\\":\\\"^[A-z0-9]{1,8}$\\\",\\\"validationError\\\":\\\"Must be alphanumeric between 1 and 8 characters.\\\",\\\"padding\\\":\\\" \\\",\\\"selected\\\":true},{\\\"name\\\":\\\"Tristam\\\",\\\"description\\\":\\\"Fill in Tristam's Name\\\",\\\"output\\\":\\\"Ninja\\\",\\\"mode\\\":\\\"text\\\",\\\"validationRegex\\\":\\\"^[A-z0-9]{1,8}$\\\",\\\"validationError\\\":\\\"Must be alphanumeric between 1 and 8 characters.\\\",\\\"padding\\\":\\\" \\\",\\\"selected\\\":true},{\\\"name\\\":\\\"Reuben\\\",\\\"description\\\":\\\"Fill in Reuben's Name\\\",\\\"output\\\":\\\"Sandwich\\\",\\\"mode\\\":\\\"text\\\",\\\"validationRegex\\\":\\\"^[A-z0-9]{1,8}$\\\",\\\"validationError\\\":\\\"Must be alphanumeric between 1 and 8 characters.\\\",\\\"padding\\\":\\\" \\\",\\\"selected\\\":true},{\\\"name\\\":\\\"Phoebe\\\",\\\"description\\\":\\\"Fill in Phoebe's Name\\\",\\\"output\\\":\\\"Baebee\\\",\\\"mode\\\":\\\"text\\\",\\\"validationRegex\\\":\\\"^[A-z0-9]{1,8}$\\\",\\\"validationError\\\":\\\"Must be alphanumeric between 1 and 8 characters.\\\",\\\"padding\\\":\\\" \\\",\\\"selected\\\":true}],\\\"checksum\\\":{\\\"description\\\":\\\"Final Fantasy Mystic Quest (NA)\\\",\\\"checksum\\\":\\\"f7faeae5a847c098d677070920769ca2\\\",\\\"script\\\":\\\"\\\",\\\"version\\\":\\\"1.1\\\"}}\"}",
        'httpMethod': 'POST'
    }
    print(lambda_handler(testEvent, 'test'))
